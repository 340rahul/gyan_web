var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require("mongoose");

var routes = require('./routes/routes.js');
var questions = require('./routes/questions');

var app = express();
app.use(cookieParser());

//Diable x-powered-by header:
app.use(function (req, res, next) {
  res.removeHeader("x-powered-by");
  next();
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, '/public')));
app.use('/bower_components', express.static(__dirname + '/bower_components'));  //bower
app.use('/', routes);
app.use('/', questions);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

var env = process.env.NODE_ENV || 'production';
var config = require('./config')[env];

//Connecting to DB:
mongoose.connect('mongodb://' + config.database.host + '/' + config.database.db, function(err) {
    if (err) {
        throw err;
    }
});


//Starting server:
app.listen(config.server.port, function() {
    console.log('Gyan web listening on port ' + config.server.port + "!");
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;