var config = {
	development: {
		database: {
			host: '127.0.0.1',
			port: '27017',
			db: 'gyanweb_dev' 
		},
		server: {
			host: '127.0.0.1',
			port: '3400'
		}
	},
    production: {
        database: {
            host: '127.0.0.1',
            port: '27017',
            db: 'gyanweb' 
        },
        server: {
            host: '127.0.0.1',
            port: '3400'
        }
    }   	
};

module.exports = config;