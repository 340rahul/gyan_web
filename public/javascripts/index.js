(function (window, document, undefined) {
	window.onload = function () {
		//loading font using Google WebFont API to ensure fonts are loaded before execution of code:
		var fontScript = document.createElement('script');
		fontScript.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		fontScript.type = 'text/javascript';
		fontScript.async = 'true';
		s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(fontScript, s);
		WebFontConfig = {
			custom: {
				families: ['Jockey One']
				, urls: ['https://fonts.googleapis.com/css?family=Jockey+One']
			}
			, //function to be executed after the fonts are loaded:
			active: function () {
				var friction = 0.95;
				var spacing = 1;
				var ease = 0.5;
				var size = 1;
				var color = "#FFFFFF";
				var radius = 1600;
				var text = "Gyan Web";
				var width = Kana.getCanvasDimensions().width;
				var height = Kana.getCanvasDimensions().height;
				var textWidth;
				Kana.setEnvironment(friction, size, spacing, ease, color);
				Kana.setMouse(0, 0, radius);
				Kana.setText(text, "", "96", "Jockey One");
				//custom particle object
				function particle(x, y) {
					this.x = this.originX = x;
					this.y = this.originY = y;
					this.rx = 0
						, this.ry = 0
						, this.vx = 0;
					this.vy = 0;
					this.force = 0;
					this.angle = 0;
					this.distance = 0;
				};
				//custom update method for default animation:
				var update = function () {
					var mouse = Kana.getMouse();
					var environment = Kana.getEnvironment();
					this.rx = mouse.x - (this.x + Kana.getCanvasBounds().BOUND_X);
					this.ry = mouse.y - (this.y + Kana.getCanvasBounds().BOUND_Y);
					this.distance = (this.rx * this.rx) + (this.ry * this.ry);
					this.force = -(mouse.radius) / this.distance;
					if (this.distance < mouse.radius) {
						this.angle = Math.atan2(this.ry, this.rx);
						this.vx += this.force * Math.cos(this.angle);
						this.vy += this.force * Math.sin(this.angle);
						//Making canvas a clickable link:
						document.body.style.cursor = "pointer";
					}
					this.x += (this.vx *= environment.friction) + (this.originX - this.x) * environment.ease;
					this.y += (this.vy *= environment.friction) + (this.originY - this.y) * environment.ease;
				};
				Kana.setParticleObject(particle);
				Kana.setUpdateParticleMethod(update);
				Kana.particlize("center", "middle", width / 2, height / 2);
				textWidth = Kana.getTextWidth();
				var animate = function () {
						Kana.update();
						Kana.render();
						window.requestAnimationFrame(animate);
					}
					//Adding event handelers:
				Kana.addWindowResizeHandler(function () {}, false);
				var mutationObserver = new MutationObserver(function (mutations) {
					mutations.forEach(function (mutation) {
						Kana.recalculateBounds();
					});
				});
				mutationObserver.observe(document.body, {
					childList: true
					, subtree: true
				});
				//Mouse event listeners:
				Kana.getCanvas().addEventListener('mousemove', function (event) {
					Kana.setMouseCoordinates(event.clientX, event.clientY);
				});
				Kana.getCanvas().addEventListener('click', function (event) {
					window.location = window.location.href + 'quiz';
				});
				Kana.getCanvas().addEventListener('mouseleave', function (event) {
					Kana.setMouseCoordinates(0, 0);
				});
				//Touch event listeners:
				Kana.getCanvas().addEventListener('touchend', function (event) {
					event.preventDefault();
					Kana.setMouseCoordinates(0, 0);
					window.location = window.location.href + 'quiz';
				}, false);
				var target;
				Kana.getCanvas().addEventListener('touchstart', function (event) {
					event.preventDefault();
					var touch = event.touches[0];
					target = document.elementFromPoint(touch.PageX, touch.PageY);
					Kana.setMouseCoordinates(event.changedTouches[0].clientX, event.changedTouches[0].clientY);
				}, false);
				Kana.getCanvas().addEventListener('touchmove', function (event) {
					event.preventDefault();
					var touch = event.touches[0];
					if (target !== document.elementFromPoint(touch.PageX, touch.PageY)) {
						//Touch left the target DOM:
						Kana.setMouseCoordinates(0, 0);
					}
					else {
						Kana.setMouseCoordinates(event.targetTouches[0].clientX, event.targetTouches[0].clientY);
					}
				}, false);
				animate();
			}
		};
	}
})(window, document);