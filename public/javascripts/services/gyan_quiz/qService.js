(function (){
    
    var qService = function($http){
        
        var getQuestions = function(category, difficulty, batchSize){
            var url = 'http://gyan.futuretraxex.com/getQuestions';
            
            if((category != null && category != '') || difficulty != null || batchSize != null){

                url += '?';

                if(category != null && category != ''){
                    url += 'category=' + category;
                }
                if(difficulty != null){
                    url += '&difficulty=' + difficulty;
                }
                if(batchSize != null){
                    url += '&batchSize=' + batchSize;
                }
            }

            return  $http.get(url).then(function(response){
                return response.data;    
            },
            function(err){
                return err;
            });
        };

        var getQuestionCategories = function(){
            var url = 'http://gyan.futuretraxex.com/getQuestionCategories';

            return $http.get(url).then(function(response){
                return response.data;
            },
            function(err){
                return err;
            });
        };

        var getAnswer = function(id){
            var url = 'http://gyan.futuretraxex.com/getAnswer?id=';
            url += id;

            return $http.get(url).then(function(response){
                return response.data;
            }, function(error){
                return error;
            });
        };

        var getDifficulties = function(category){
            var url = "http://gyan.futuretraxex.com/getDifficulties?category=";
            url += category;

            return $http.get(url).then(function(response){
                return response.data;
            }, function(error){
                return error;
            });
        };
        
        return{
            getQuestions: getQuestions,
            getQuestionCategories: getQuestionCategories,
            getAnswer: getAnswer,
            getDifficulties: getDifficulties
        };
    };
    
    var module = angular.module('Gyan');
    module.factory('qService', qService);
    
})();