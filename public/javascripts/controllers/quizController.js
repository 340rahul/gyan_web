(function() {

	var app = angular.module('Gyan', ['ngRoute', 'ngAnimate']);

	var quizController = function($scope, qService, $document, $timeout, $interval) {

		//g:
		var quizCard = $('#quizCard');
		var ioContainer = $('#ioContainer');
		var startContainer = $('#startContainer');
		var cardBackgroundColor = quizCard.css('background-color');
		var correctAnswerColor = '#00B32D';
		var wrongAnswerColor = '#FF4B3F';
		var hoverOptionColor = '#2F50A3';

		$scope.quizStarted = false;
		$scope.outputRequired = false;
		$scope.quizCompleted = false;
		$scope.resetBtnRequired = false;
		$scope.noChangeReq = false; // For question changing transitions
		$scope.errorOutputRequired = false;

		//For menu nav:
		$scope.currentScreen = 'startScreen';
		$scope.screenStack = ['startScreen'];

		$scope.questions = [];
		$scope.categories = [];
		$scope.difficulty = ["Random", "Easy", "Moderate", "Hard"];
		$scope.difficultyRef = ["Random", "Easy", "Moderate", "Hard"];
		$scope.batchSize = 5;
		$scope.totalPoints = 0;

		$scope.user = {
			name: 'anonymous',
			category: 'Random',
			difficulty: 'Easy',
			currentQuestion: 0,
			firstClickOnOptions: true,
			score: 0
		};

		var init = function() {
			$scope.quizStarted = false;
			$scope.outputRequired = false;
			$scope.quizCompleted = false;
			$scope.resetBtnRequired = false;
			$scope.noChangeReq = false; // For question changing transitions
			$scope.errorOutputRequired = false;

			//For menu nav:
			$scope.currentScreen = 'startScreen';
			$scope.screenStack = ['startScreen'];

			$scope.questions = [];
			$scope.categories = [];
			$scope.difficulty = ["Random", "Easy", "Moderate", "Hard"];
			$scope.difficultyRef = ["Random", "Easy", "Moderate", "Hard"];
			$scope.batchSize = 5;
			$scope.totalPoints = 0;

			$scope.user = {
				name: 'anonymous',
				category: 'Random',
				difficulty: 'Easy',
				currentQuestion: 0,
				firstClickOnOptions: true,
				score: 0
			};
			startContainer.slideDown(function() {
				$('#startBtn').slideDown();
				$('#rulesBtn').slideDown(function() {
					$scope.$apply();
				});
			});
		};

		$document.ready(function() {
			init();
		});

		$scope.backBtnClicked = function() {
			$scope.screenStack.pop();
			if ($scope.screenStack[$scope.screenStack.length - 1] == 'startScreen') {
				ioContainer.slideUp(function() {
					$scope.currentScreen = 'startScreen';
					$scope.$apply();
					startContainer.slideDown(function() {
						startContainer.css('display', 'flex');
						$('#startBtn').slideDown(function() {
							$('#rulesBtn').slideDown();
						});
					});
				});
			} else if ($scope.screenStack[$scope.screenStack.length - 1] == 'rulesScreen') {
				$scope.currentScreen = 'rulesScreen';
			}
		};

		$scope.startBtnClicked = function() {
			$('#startBtn').slideUp(function() {
				$('#rulesBtn').slideUp(function() {
					startContainer.slideUp(function() {
						qService.getQuestionCategories().then(onCategoriesFetched, $scope.onError);
					});
				});
			});
		};

		var onCategoriesFetched = function(categories) {
			categories.push('Random');
			$scope.categories = categories;
			$scope.currentScreen = 'categorySelectScreen';
			$scope.screenStack.push($scope.currentScreen);
			ioContainer.slideDown();
		};

		$scope.getDifficulties = function() {
			if ($scope.user.category == 'Random') {
				$scope.difficulty = ["Random", "Easy", "Moderate", "Hard"];
			} else {
				qService.getDifficulties($scope.user.category).then(onDifficultiesFetched, $scope.onError);
			}
		};

		var onDifficultiesFetched = function(difficulties) {
			$scope.difficulty = [];
			for (var i = 0; i < difficulties.length; i++) {
				var d = difficulties[i];
				if (d == 1) {
					$scope.difficulty.push("Easy");
				} else if (d == 2) {
					$scope.difficulty.push("Moderate");
				} else {
					$scope.difficulty.push("Hard");
				}
			}

			if ($scope.difficulty.length > 1) {
				$scope.difficulty.unshift("Random");
			}
		};

		$scope.rulesBtnClicked = function() {
			$('#startBtn').slideUp(function() {
				$('#rulesBtn').slideUp(function() {
					startContainer.slideUp(function() {
						$scope.currentScreen = "rulesScreen";
						$scope.screenStack.push($scope.currentScreen);
						$scope.$apply();
						ioContainer.slideDown();
					});
				});
			});
		};

		$scope.nextBtnClicked = function() {

			if ($scope.currentScreen == 'rulesScreen') {
				qService.getQuestionCategories().then(onCategoriesFetched, $scope.onError);
			} else if ($scope.currentScreen == 'categorySelectScreen') {

				if ($scope.user.difficulty != null && $scope.user.category != null) {
					$scope.errorOutputRequired = false;
					qService.getQuestions($scope.user.category, $scope.difficultyRef.indexOf($scope.user.difficulty), $scope.batchSize).then(onQuestionsFetched, $scope.onError);
				} else {
					ioContainer.slideUp(function() {
						$scope.errorOutputRequired = true;
						$('#errorOutput').html("ERROR: Please select a valid difficulty, and category.");
						$scope.$apply();
						ioContainer.slideDown();
					})
				}
			}
		};

		var onQuestionsFetched = function(questions) {
			//!ISSUE:
			if (questions.length == 0) {
				setTimeout(function() {
					qService.getQuestions($scope.user.category, $scope.difficultyRef.indexOf($scope.user.difficulty), $scope.batchSize).then(onQuestionsFetched, $scope.onError);
				}, 100);
				return;
			} else {
				if ($scope.batchSize > questions.length) {
					$scope.batchSize = questions.length;
				}
				$scope.questions = questions;
				//calculate total possible points:
				for (var i = 0; i < $scope.questions.length; i++) {
					$scope.totalPoints += $scope.questions[i].difficulty;
				}
				displayQuestion();
			}
		};

		var displayQuestion = function() {
			$scope.noChangeReq = false;
			var opt = ['A.', 'B.  ', 'C.  ', 'D.  '];
			var currentQuestion = $scope.questions[$scope.user.currentQuestion];
			$(ioContainer).slideUp(function() {
				$('#textContainer').text(currentQuestion.text).qtip({
					content: {
						text: 'Category: ' + currentQuestion.category + ', Difficulty: ' + $scope.difficultyRef[currentQuestion.difficulty]
					},
					position: {
						my: 'bottom center',
						at: 'top center'
					},
					style: {
						classes: 'qtip-light',
						tip: false
					}
				});
				for (var i = 0; i < currentQuestion.options.length; i++) {
					var str = '#option' + (i + 1).toString();
					$(str).append(opt[i]).html('&nbsp').append(opt[i] + currentQuestion.options[i]);
				}

				$scope.currentScreen = 'quizScreen';
				$scope.screenStack.push($scope.currentScreen);
				$scope.outputRequired = true;
				$scope.quizStarted = true;
				ioContainer.slideDown();
				$scope.user.firstClickOnOptions = true;
				$scope.noChangeReq = true;
				$scope.$apply();
			});
		};

		$scope.optionClicked = function(event) {
			if ($scope.user.firstClickOnOptions) {
				$scope.user.answerSubmitted = $(event.currentTarget).attr('value');
				$scope.user.firstClickOnOptions = false;
				qService.getAnswer($scope.questions[$scope.user.currentQuestion]._id).then(onAnswerFetched, $scope.onError);
			}
		};

		var onAnswerFetched = function(answer) {
			answer = answer[0].answer
			var str = '#optionDiv' + $scope.user.answerSubmitted;
			var target = $(str);
			if (answer == $scope.user.answerSubmitted) {

				//Correct answer:
				$scope.user.score += 1 * ($scope.questions[$scope.user.currentQuestion].difficulty);
				target.css("background", "linear-gradient(to left, " + hoverOptionColor + " 98%, " + correctAnswerColor + " " + "2%)");
				var r = Math.round(100 - (($scope.user.score / $scope.totalPoints) * 100));
				if (r < 2) {
					$('.gyan-card-output').css('background-color', correctAnswerColor);
					$('.gyan-card-output').css('background', correctAnswerColor);
				} else {
					$('.gyan-card-output').css("background", "linear-gradient(to left, " + wrongAnswerColor + " " + r + "%, " + correctAnswerColor + " " + "2%)");
				}
			} else {

				//Wrong answer:
				target.css("background", "linear-gradient(to left, " + hoverOptionColor + " 98%, " + wrongAnswerColor + " " + "2%)");
				str = '#optionDiv' + answer;
				var correctTarget = $(str);
				correctTarget.css("background", "linear-gradient(to left, " + cardBackgroundColor + " 98%," + correctAnswerColor + " " + "2%)");
			}
			++$scope.user.currentQuestion;
			$timeout(function() {
				$('.options').css('background', cardBackgroundColor).css('background-color', cardBackgroundColor).hover(function() {
					$(this).css('background-color', hoverOptionColor);
				}, function() {
					$(this).css('background-color', cardBackgroundColor);
				});
				if ($scope.user.currentQuestion < $scope.batchSize) {
					//Questions remaining:
					displayQuestion();
				} else {

					//Quiz Over:
					ioContainer.slideUp(function() {
						$scope.quizCompleted = true;
						$scope.resetBtnRequired = true;
						$scope.outputRequired = false;
						$scope.$apply();
						ioContainer.slideDown();
						animateChart($('#chartCover'), ($scope.user.score / $scope.totalPoints) * 100, 3);
					});
				}
			}, 1500);
		};

		var animateChart = function(element, value, time) {

			value = (value / 100) * 360;

			var rotation = 0;

			$interval(function() {
				if (value >= rotation) {
					++rotation;
					var number = Math.round((rotation / 360) * 100);
					$("#hole").html("<span style='color: whitesmoke;'>" + number + "%</span>");
					$('#scoreHolder').html(number);
					if (rotation <= value && rotation <= 180) {
						element.css("transform", "rotate(" + rotation + "deg" + ")");
					} else if (rotation < value && rotation > 180) {
						element.css("transform", "rotateX(180deg)");
						element.css("background-color", "#00B32D");
						element.css("transform", "rotate(" + (rotation - 180) + "deg" + ")");
					}
				}
			}, time);
		};

		$scope.resetBtnClicked = function() {
			$('#chartCover').css("transform", "");
			$('#chartCover').css('background-color', wrongAnswerColor);
			ioContainer.slideUp(function() {
				$('#cardOutput').css("background", wrongAnswerColor).css("background-color", wrongAnswerColor);
				init();
			});
		};

		$scope.onError = function(err) {
			console.log("ERROR: ", err);
		};
	};
	app.controller('quizController', quizController);

})();