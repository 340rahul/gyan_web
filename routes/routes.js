var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index.pug', { title: 'Gyan Web' });
});

module.exports = router;

router.get('/quiz', function(req, res, next){
    res.render('quiz.pug', {title: 'Gyan Web'});
});