/*get questions from db*/
var express = require('express');
var router = express.Router();
var Quiz = require('../QuizQuestions.model');
var mongoose = require('mongoose');

function error_handler(res, message, code) {
    console.log('ERROR: ' + message);
    res.status(code || 500).json({
        'error': message
    });
}

//Expects Difficulty, batch size, and category as parameters.
//Returns question with random difficulty/category if not provided.
//Returns 1 question if batch size not provided.
//Example: /getQuestions?category=Biology&difficulty=1&batchSize=20
router.get('/getQuestions', function(req, res) {
    var query = [];
    var difficulty = parseInt(req.query.difficulty) || Math.round(Math.random() * 3);
    var batchSize = parseInt(req.query.batchSize, 10) || 1;
    var category = req.query.category;

    if(batchSize > 20 && req.query.batchSize < 1){
        error_handler(res, "Error: Please provide a valid batchSize.");
        return;
    }

    if(category == 'Random'){
        category = null;
    }

    if(difficulty == 'Random' || difficulty == 0){
        difficulty = null;
    }

    //NOTE: For aggregate arguments, 
    //Mongoose won't do schema casting of objects automatically:
    if(category != null && difficulty != null){
        query.push({
            $match: 
            Quiz.where({
                difficulty:   difficulty,
                category  :   category 
            }).cast(Quiz)
        });
    }else if(category == null && difficulty != null){
        query.push({
            $match: 
            Quiz.where({
                            difficulty:   difficulty,
            }).cast(Quiz)
        });
    }

    query.push({
        $sample: {
            size    :   batchSize
        }
    });

    query.push({
        $project: {
            text: 1,
            options: 1,
            category: 1,
            difficulty: 1
        }
    });

    Quiz.aggregate(query, function(error, question){
        if(error){
            error_handler(res, error);
        }
        res.json(question);
    });
});

//Get all the available categories:
router.get('/getQuestionCategories', function(req, res){
    Quiz.distinct("category", function(error, categories){
        if(error){
            error_handler(res, error);
        }
        res.json(categories);
    });
});

//Given the _id returns the corresponding answer:
router.get('/getAnswer', function(req, res){
    var id = mongoose.Types.ObjectId(req.query.id);

    Quiz.find({_id: id}, {answer: 1, _id: 0}, function(error, answer){
        if(error){
            error_handler(res, error);
        }
        res.json(answer);
    }); 
});

//Given the category returns the available difficulties:
router.get('/getDifficulties', function(req, res){
    Quiz.distinct("difficulty", {
        category: req.query.category
    }, function(error, difficulties){
        if(error){
            error_handler(res, error);
        }
        res.json(difficulties);
    });
})

module.exports = router;