var mongoose = require("mongoose");
var Schema = mongoose.Schema;


var quizQuestionsSchema = new Schema({
    text: {
        type: String,
        unique: true,
        required: true,
        dropDups: true
    },
    difficulty: {
        type: Number,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    options: {
        type: Array,
        required: true
    },
    answer: {
        type: Number,
        required: true
    },
    created_at: Date,
    updated_at: Date
});

quizQuestionsSchema.index({
    text: 1,
    category: 1,
    difficulty: 1
}, {
    unique: true
})

var QuizQuestions = mongoose.model("QuizQuestion", quizQuestionsSchema);

module.exports = QuizQuestions;